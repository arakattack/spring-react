<pre>
app-server_1  | 
app-server_1  |   .   ____          _            __ _ _
app-server_1  |  /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
app-server_1  | ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
app-server_1  |  \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
app-server_1  |   '  |____| .__|_| |_|_| |_\__, | / / / /
app-server_1  |  =========|_|==============|___/=/_/_/_/
app-server_1  |  :: Spring Boot ::        (v2.2.1.RELEASE)
</pre>

# Spring Boot Template

Spring Boot Template adalah **full stack project** untuk membangun aplikasi web berbasis Java. Template memudahkan developer laravel untuk [devops](https://en.wikipedia.org/wiki/DevOps), otomasi CI/CD.
Fitur:

- aplikasi fullstack yang terdiri dari `nginx proxy`, `postgresql`, `spring boot`, `tomcat`, `react`.
- auto devops staging environment untuk review UAT
- auto devops production environmen
- 0 downtime

![App Screenshot](screenshot.png)

## Installation

1. **Import project** https://gitlab.com/arakattack/spring-react.git dengan metode Git repository URL ke `$repo_project_baru`. **New project → Import project → Git repository URL**

2. Di **GitLab repository → Settings → CI/CD → Runners page**, lihat ‘Specific Runners’, informasi url & token di bagian 'Set up a specific Runner manually' akan digunakan untuk register `gitlab-runner` di server sebagai berikut.

## Setup Docker Server

Project ini menggunakan 2 server: staging & production.

1. Install kedua server dengan OS ubuntu/debian.
2. Install gitlab-runner

```bash
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
$ sudo apt-get install gitlab-runner
```

3. Daftarkan gitlab-runner

```bash
$ sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$token" \
  --executor "shell" \
  --docker-image alpine:latest \
  --description "$nama_server" \
  --tag-list "$nama_branch" \
  --pre-clone-script "sudo chown -R gitlab-runner:gitlab-runner ."
```

4. Tambahkan user gitlab-runner ke sudoers

```bash
$ sudo echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
```

5. Install docker

```bash
$ sudo apt-get install docker.io
```

## Usage

1. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) di komputer (jika belum), kemudian clone repository:

```bash
$ git clone https://gitlab.com/$repo_project_baru
$ cd $repo_project_baru
$ git checkout staging
$ ./install.sh
$ git add .
$ git commit -am "master commit"
$ git push
```

2. Ganti password yang ditemukan di `.env.example`, `docker-compose.yml`
3. Edit `.gitlab-ci.yml` bagian `environment url`

## Development

### Branch

Branch digunakan untuk mengatur code WIP (Work In Progress) digunakan untuk mengisolasi kode dalam pengerjaan sebelum dikirim ke staging dan ke master (production). Repo ini menggunakan branch **staging** sebagai default branch.

#### Branch Developer

1. Setiap developer harus membuat branch sendiri
   > Pastikan selalu ambil dari branch staging

```bash
$ git checkout staging
$ git checkout -b nama-developer
```

...coding

#### Branch Issue/Hotfix

2. Untuk menghubungkan gitlab issue secara otomatis, buat branch untuk perbaikan tersebut dengan cara membuat branch sesuai id issue yang akan ditangani, contoh: `{id-issue}-nama-issue`

```bash
$ git checkout staging
$ git checkout -b 1-nama-issue
```

...coding

#### Review /Merge Branch

3. Untuk mengirimkan code dari branch 1-login-issue ke branch staging, gunakan perintah sebagai berikut:

```bash
$ git add .
$ git commit -am "penjelasan detil"
$ git push origin 1-nama-issue -o merge_request.create -o merge_request.target=staging
$ git branch -d 1-nama-issue
```

Pada langkah ini, issue otomatis akan diedit menjadi `WIP Nama Issue` dan akan diclose setelah merge

4. Untuk menyetujui merge, gunakan perintah sebagai berikut:

```bash
$ git checkout staging
$ git merge 1-nama-issue
```

> Pada langkah ini, code otomatis dikirimkan ke server staging. Bisa dicek live di https://staging.$domain

5. Untuk mengirimkan dari branch staging ke branch master, gunakan perintah sebagai berikut:

```bash
$ git checkout staging
$ git add .
$ git commit -am "penjelasan detil"
$ git push -o merge_request.create
```

> Lebih lanjut mohon pelajari https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging 6. Untuk menyetujui merge request dari branch staging ke branch master, dilakukan melalui gitlab karena deployment ke server production harus menggunakan tombol deploy manual di `pipeline`

> Pada langkah ini, code otomatis dikirimkan ke server production setelah tombol deploy dijalankan. Bisa dicek live di https://\$domains

> <span style="color:red">Branch master tidak boleh dicheckout untuk diedit karena menggunakan mekanisme merge request.</span>

## Deployment

Setelah staging selesai, saatnya rilis versi sekaligus release production.

```bash
$ git tag vX.X.X
```

### Local

Install [docker](https://docs.docker.com/get-docker/) & [docker-compose](https://docs.docker.com/compose/install/).

```bash
$ docker-compose up --build
$ docker ps
```

#### Akses management console

Untuk mengakses Carbon Management Console, gunakan IP host Docker dan port 9443 sebagai berikut:

```bash
https://{DOCKER_HOST}:9443/carbon
```

Untuk mengakses API Manager Publisher, gunakan IP host Docker dan port 9443 sebagai berikut:

```bash
https://{DOCKER_HOST}:9443/publisher
```

Untuk mengakses Portal Pengembang API Manager, gunakan IP host Docker dan port 9443 sebagai berikut:

```bash
https://{DOCKER_HOST}:9443/devportal
```

Untuk mengakses front end, gunakan IP host Docker dan port 80 sebagai berikut:

```bash
http://{DOCKER_HOST}
```

Untuk mengakses layanan database Postgres, gunakan IP host Docker dan port 5432.

### Docker Server

Pada saat developer `git push -o merge_request.create`, ada 2 aksi yang dilakukan sekaligus, yaitu:

1. Code akan otomatis dikirim ke server staging
2. Merge request dari branch staging ke master

   - Merge request boleh dijalankan jika code dianggap sukses di server staging.
   - Ketika merge request dijalankan, deployment ke server production bisa dijalankan.
   - Jika terjadi kesalahan, bisa dirollback melalui tomboll rollback.

### Openshift

Sama dengan Docker Server

## Todo List

- [ ] Hardening script
- [ ] Letsencrypt ssl
- [x] WSO2 Api Manager
- [ ] gitlab-ci.yml

## License

[MIT](https://choosealicense.com/licenses/mit/)
